import 'dart:io';

void main() {
  print("Enter Farenheit =");
  String? farenheit = stdin.readLineSync();
  double tempFarenheit = double.parse(farenheit!);
  double tempCelsius = (tempFarenheit - 32) / 1.8;
  print("tempFarenheit = $tempCelsius");
}