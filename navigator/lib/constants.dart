import 'package:flutter/material.dart';

const kPrimaryColor = Colors.deepPurple;
const kPrimaryLightColor = Colors.amber;

const double defaultPadding = 16.0;
