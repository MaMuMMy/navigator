import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../constants.dart';

class SignUpScreenTopImage extends StatelessWidget {
  const SignUpScreenTopImage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          "SIGN UP",
          style: TextStyle(
            fontSize: 38,fontWeight: FontWeight.bold,color: Colors.pink,
          ),
        ),

        Text(
          "sign up plz",
          style: TextStyle(
            fontSize: 18,
          ),
        ),

        SizedBox(height: defaultPadding),
        Row(
          children: [
            const Spacer(),
            Expanded(
              flex: 8,
              child: Icon(Icons.accessible_forward,size: 150,color: Colors.amber,),
            ),
            const Spacer(),
          ],
        ),
        SizedBox(height: defaultPadding),
      ],
    );
  }
}
