import 'package:flutter/material.dart';
import 'package:nav_app/constants.dart';

class RegisterSucessPage extends StatelessWidget {
  const RegisterSucessPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          "successfully registered.",
          style: TextStyle(
            fontSize: 20,
            color: Colors.pinkAccent,
          ),
        ),
        Padding(padding: EdgeInsets.all(4)),
        Text(
          "เสร็จสักที",
          style: TextStyle(
            fontSize: 38,
            fontWeight: FontWeight.bold,
            color: Colors.deepPurple,
          ),
        ),
        SizedBox(height: defaultPadding * 2),
        Row(
          children: [
            Spacer(),
            Expanded(
              flex: 8,
              child: Icon(
                Icons.accessibility_sharp,
                size: 200,
                color: Colors.pinkAccent,
              ),
            ),
            Spacer(),
          ],
        ),
        SizedBox(height: defaultPadding * 2),
      ],
    );
  }
}
