import 'package:flutter/material.dart';
import 'package:nav_app/Screens/Welcome/welcome_screen.dart';

class BackBtn extends StatelessWidget {
  const BackBtn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Hero(
          tag: "back_btn",
          child: ElevatedButton(
            onPressed: () {
              Navigator.of(context).popUntil((route) => route.isFirst);
            },
            child: Text(
              "SIGN OUT".toUpperCase(),
            ),
            style: ElevatedButton.styleFrom(
              primary: Colors.deepPurple,
              onPrimary: Colors.pinkAccent, // Background color
            ),
          ),
        ),
        const SizedBox(height: 16),
      ],
    );
  }
}

